# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all
Group.destroy_all

user_list = [
  [ 'Admin1', 'admin1@example.com', 'admin1234' ],
  [ 'User1', 'user1@example.com', 'user1234' ],
  [ 'User2', 'user2@example.com', 'user1234' ],
  [ 'User3', 'user3@example.com', 'user1234' ]
]

# create some users
user_list.each do |name, email, password|
  User.create!( name: name, email: email, password: password )
  puts "Created User with name: #{name}, email: #{email}, password: #{password}"
end

group_list = [ 'admin', 'member', 'user' ]

# create some groups
group_list.each do |name|
  Group.create!( name: name )
  puts "Created group with name: #{name}"
end

# assign some groups to some users
admin1 = User.find_by_name('Admin1')
user1 = User.find_by_name('User1')
user3 = User.find_by_name('User3')

group1 = Group.find_by_name('admin')
group2 = Group.find_by_name('member')
group3 = Group.find_by_name('user')

admin1.groups << group1
user1.groups = [group2, group3]
user3.groups << group2

def print_group_assignment user
  puts "Assigned User: #{user.name} to Group: #{user.groups.map(&:name)}"
end

[admin1, user1, user3].each do |user|
  user.save!
  print_group_assignment user
end
