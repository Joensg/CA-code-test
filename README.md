## User registration JSON API

Steps to get the application up and running.

* Ruby version
`ruby 2.3.3`

* Database creation
`rake db:create db:migrate`

* Database initialization
`rake db:seed`

* How to run the test suite
```
bundle install
bundle exec rspec
```

* Start server
```
bundle exec rails s
```

### Requirement guidelines
To test the requirements, on the server, besides rspec, please use the following curl examples as a guide.
1) User registration:
```
curl -v -X POST \
  http://localhost:3000/auth \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "email": "testuser@example.com",
    "name": "testuser",
    "password": "testuser1234"
}'

```
2) User sign in:
```
curl -v -X POST \
  http://localhost:3000/auth/sign_in \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "email": "testuser@example.com",
    "password": "testuser1234"
}'

```
Returns with valid token in the response headers. Use these for future requests as follows.

3) Users with valid token, and assigned to admin group, can create a new group and assign users to groups:

- create new group, with group name provided:
```
curl -v -X POST \
  http://localhost:3000/authn/group \
  -H 'access-token: g2fz4PIh8C9rC7JKx0IT1A' \
  -H 'token-type: Bearer' \
  -H 'Client: gTo_IjlrU_eSw1M1ik4gMA' \
  -H 'Uid: admin1@example.com' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "testgroup"
}'

```

- assign group to user, with group_name and user_name provided:
```
curl -v -X POST \
  http://localhost:3000/authn/user_groups \
  -H 'access-token: g2fz4PIh8C9rC7JKx0IT1A' \
  -H 'token-type: Bearer' \
  -H 'Client: gTo_IjlrU_eSw1M1ik4gMA' \
  -H 'Uid: admin1@example.com' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "group_name": "testgroup",
    "user_name": "User1"
}'

```

4) Other applications with valid token, supplying user_name or email_address, can request the users data:

- With user_name provided:
```
curl -v -X POST \
  http://localhost:3000/authn/user_details \
  -H 'access-token: g2fz4PIh8C9rC7JKx0IT1A' \
  -H 'token-type: Bearer' \
  -H 'Client: gTo_IjlrU_eSw1M1ik4gMA' \
  -H 'Uid: testuser@example.com' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "user_name": "testuser"
}'

```

- With email_address provided:
```
curl -v -X POST \
  http://localhost:3000/authn/user_details \
  -H 'access-token: g2fz4PIh8C9rC7JKx0IT1A' \
  -H 'token-type: Bearer' \
  -H 'Client: gTo_IjlrU_eSw1M1ik4gMA' \
  -H 'Uid: testuser@example.com' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "email_address": "testuser@example.com"
}'

```

### Rspec output
```
AuthnController
  GET #whoami
    returns http success
  GET #checkme
    returns http unauthorized

Group
  group assigned users
    has users
  group with name
    is unique

User
  user assigned groups
    has groups
  #is_admin?
    user assigned to admin group
      returns true
    user assigned to non-admin group
      returns false
  #create_group
    user assigned to admin group
      can create new groups
    user assigned to non-admin group
      cannot create new groups
  #assign_group
    user assigned to admin group
      can assign other users to groups
    user assigned to non-admin group
      cannot assign other users to groups
  #details
    returns with username, email address and group memberships

Authentication Api
  sign-up
    valid registration
      successfully creates account
    invalid registration
      missing information
        reports error with messages
      non-unique information
        reports non-unique e-mail
  anonymous user
    accesses unprotected
    fails to access protected resource
  login
    valid user login
      generates access token
      extracts access headers
      grants access to resource
      grants access to resource multiple times
      logout
    invalid password
      rejects credentials
    test access tokens of varying ages
      token age is within 1 hour
        uses the given parameter
      token age is over 1 hour
        uses the given parameter
      token age is within 2 hours
        uses the given parameter
      token age is over 2 hours
        uses the given parameter
  create new group
    with valid token and assigned to admin group
      when name is provided
        returns with group details
    with invalid token
      fails to access user details
  assign user to group
    with valid token and assigned to admin group
      when group_name and user_name is provided
        returns with http success
    with invalid token
      fails to access user details
  get user_details for user
    with valid token
      when user_name is provided
        returns with user details
      when email_address is provided
        returns with user details
      when matching user is not found
        returns with an empty hash
    with invalid token
      fails to access user details

Finished in 6.28 seconds (files took 2.82 seconds to load)
35 examples, 0 failures
```
### Future enhancements
Given more time, some of the future enhancements would be
- writing some integration tests around the following scenarios
    - a user signing up and then logging in
    - an admin user logging in and then creating new groups
    - an admin user logging in and then assigning a group to a user
    - a third party application with a valid user token, being able to fetch details about a user.
- adding in some views to be able to easily interact with the api, for user sign up, login, group creation and group assignment, ideally using a front-end framework.
