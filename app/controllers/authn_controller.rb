class AuthnController < ApplicationController
  before_action :authenticate_user!, only: [:checkme, :group, :user_groups, :user_details]

  def whoami
    render json: current_user || {}
  end

  def checkme
    render json: current_user || {}
  end

  def user_details
    user_details = user&.details if current_user

    render json: user_details || {}
  end

  def group
    group = current_user.create_group(params[:name]) if current_user

    render json: group || {}
  end

  def user_groups
    user_groups = current_user.assign_group(params[:group_name], params[:user_name]) if current_user

    render json: user_groups || {}
  end

  private

  def user
    User.where(name: params[:user_name]).first ||
    User.where(email: params[:email_address]).first
  end
end
