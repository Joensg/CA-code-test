class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_and_belongs_to_many :groups

  def has_groups group_list
    group_names = groups.map(&:name)
    (group_names & group_list).any?
  end

  def is_admin?
    groups.where(name: Group::ADMIN).exists?
  end

  def create_group group_name
    Group.create!(name: group_name) if is_admin?
  end

  def assign_group(group_name, user_name)
    user(user_name)&.groups << Group.find_by_name(group_name) if user(user_name).present? & is_admin?
  end

  def details
    {
      user_name: name,
      email_address: email,
      group_memberships: groups.map(&:name)
    }
  end

  private

  def user user_name
    User.find_by_name(user_name)
  end
end
