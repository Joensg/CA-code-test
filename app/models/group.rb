class Group < ActiveRecord::Base
  ADMIN = 'admin'

  has_and_belongs_to_many :users
end
