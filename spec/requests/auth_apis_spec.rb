require 'rails_helper'

RSpec.describe "Authentication Api", type: :request do
  include_context 'db_cleanup_each', :transaction
  let(:user_props) { FactoryGirl.attributes_for(:user) }

  context 'sign-up' do
    context 'valid registration' do
      it 'successfully creates account' do
        signup user_props

        payload=parsed_body
        expect(payload).to include("status"=>"success")
        expect(payload).to include("data")
        expect(payload["data"]).to include("id")
        expect(payload["data"]).to include("provider"=>"email")
        expect(payload["data"]).to include("uid"=>user_props[:email])
        expect(payload["data"]).to include("name"=>user_props[:name])
        expect(payload["data"]).to include("email"=>user_props[:email])
        expect(payload["data"]).to include("created_at","updated_at")
      end
    end

    context 'invalid registration' do
      context 'missing information' do
        it 'reports error with messages' do
          signup user_props.except(:email), :unprocessable_entity

          payload=parsed_body
          expect(payload).to include("status"=>"error")
          expect(payload).to include("data")
          expect(payload["data"]).to include("email"=>nil)
          expect(payload).to include("errors")
          expect(payload["errors"]).to include("email")
          expect(payload["errors"]).to include("full_messages")
          expect(payload["errors"]["full_messages"]).to include(/Email/i)
        end
      end

      context 'non-unique information' do
        it 'reports non-unique e-mail' do
          signup user_props, :ok
          signup user_props, :unprocessable_entity
          
          payload=parsed_body
          expect(payload).to include("status"=>"error")
          expect(payload).to include("errors")
          expect(payload["errors"]).to include("email")
          expect(payload["errors"]).to include("full_messages")
          expect(payload["errors"]["full_messages"]).to include(/Email/i)
        end
      end
    end
  end

  context 'anonymous user' do
    it 'accesses unprotected' do
      get authn_whoami_path

      expect(response).to have_http_status(:ok)
      expect(parsed_body).to eq({})
    end

    it 'fails to access protected resource' do
      get authn_checkme_path

      expect(response).to have_http_status(:unauthorized)
      expect(parsed_body).to include("errors"=>["You need to sign in or sign up before continuing."])
    end
  end

  context 'login' do
    let(:account) { signup user_props, :ok }
    let!(:user) { login account, :ok }

    context 'valid user login' do
      it 'generates access token' do
        expect(response.headers).to include("uid"=>account[:uid])
        expect(response.headers).to include("access-token")
        expect(response.headers).to include("client")
        expect(response.headers).to include("token-type"=>"Bearer")
      end

      it 'extracts access headers' do
        expect(access_tokens?).to be true
        expect(access_tokens).to include("uid"=>account[:uid])
        expect(access_tokens).to include("access-token")
        expect(access_tokens).to include("client")
        expect(access_tokens).to include("token-type"=>"Bearer")
      end

      it 'grants access to resource' do
        jget authn_checkme_path

        expect(response).to have_http_status(:ok)

        payload=parsed_body
        expect(payload).to include("id"=>account[:id])
        expect(payload).to include("uid"=>account[:uid])
      end

      it 'grants access to resource multiple times' do
        (1..10).each do |idx|
          #sleep 6
          #quick calls < 5sec use same tokens
          jget authn_checkme_path
          expect(response).to have_http_status(:ok)
        end
      end

      it 'logout' do
        logout :ok
        expect(access_tokens?).to be false

        jget authn_checkme_path
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'invalid password' do
      it 'rejects credentials' do
        login account.merge(:password=>"badpassword"), :unauthorized
      end
    end

    shared_examples "use authentication tokens of different ages" do |token_age, http_status|      
      let(:vary_authentication_age) { token_age }
      let(:account) { signup user_props, :ok }
      let!(:user) { login account, :ok }

      it "uses the given parameter" do
        expect(vary_authentication_age(token_age)).to have_http_status(http_status)
      end

      def vary_authentication_age(token_age)
          allow(Time).to receive(:now).and_return(Time.now + token_age)   

          jget authn_checkme_path
          return response          
      end
    end

    context "test access tokens of varying ages" do
      context 'token age is within 1 hour' do
        include_examples "use authentication tokens of different ages", 59.minutes, :ok
      end

      context 'token age is over 1 hour' do
        include_examples "use authentication tokens of different ages", 61.minutes, :ok
      end

      context 'token age is within 2 hours' do
        include_examples "use authentication tokens of different ages", 119.minutes, :ok
      end

      context 'token age is over 2 hours' do
        include_examples "use authentication tokens of different ages", 121.minutes, :unauthorized
      end
    end
  end

  context 'create new group' do
    let(:admin_group) { FactoryGirl.create(:group, name: Group::ADMIN) }

    context 'with valid token and assigned to admin group' do
      let(:account) { signup user_props, :ok }
      let!(:user) { login account, :ok }

      before do
        temp_user = User.where(name: user["name"]).first
        temp_user.groups << admin_group
        temp_user.save!
      end

      context 'when name is provided' do
        subject(:create_new_group) { jpost authn_group_path, { name: 'testgroup' } }

        it 'returns with group details' do
          create_new_group

          expect(response).to have_http_status(:ok)
          expect(parsed_body["name"]).to eq('testgroup')
        end
      end
    end

    context 'with invalid token' do
      subject(:create_new_group) { jpost authn_group_path, { name: 'testgroup' } }

      it 'fails to access user details' do
        create_new_group

        expect(response).to have_http_status(:unauthorized)
        expect(parsed_body).to include("errors"=>["You need to sign in or sign up before continuing."])
      end
    end
  end

  context 'assign user to group' do
    let(:admin_group) { FactoryGirl.create(:group, name: Group::ADMIN) }
    let(:group_2) { FactoryGirl.create(:group, name: 'general_user') }
    let(:group_3) { FactoryGirl.create(:group, name: 'general_member') }
    let!(:user_2) { FactoryGirl.create(:user, groups: [group_2]) }

    context 'with valid token and assigned to admin group' do
      let(:account) { signup user_props, :ok }
      let!(:user) { login account, :ok }

      before do
        temp_user = User.where(name: user["name"]).first
        temp_user.groups << admin_group
        temp_user.save!
      end

      context 'when group_name and user_name is provided' do
        subject(:assign_user_group) { jpost authn_user_groups_path, { group_name: group_3.name, user_name: user_2.name } }

        it 'returns with http success' do
          assign_user_group

          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'with invalid token' do
      subject(:assign_user_group) { jpost authn_user_groups_path, { group_name: group_3.name, user_name: user_2.name } }

      it 'fails to access user details' do
        assign_user_group

        expect(response).to have_http_status(:unauthorized)
        expect(parsed_body).to include("errors"=>["You need to sign in or sign up before continuing."])
      end
    end
  end

  context 'get user_details for user' do
    let(:group_1) { FactoryGirl.create(:group, name: 'member') }
    let(:group_2) { FactoryGirl.create(:group, name: 'general_user') }
    let!(:user_2) { FactoryGirl.create(:user, groups: [group_1, group_2]) }

    context 'with valid token' do
      let(:account) { signup user_props, :ok }
      let!(:user) { login account, :ok }

      shared_examples 'returns with user details' do
        it 'returns with user details' do
          get_details_for_user

          expect(response).to have_http_status(:ok)
          expect(parsed_body).to eq(user_2.details.stringify_keys)
        end
      end

      context 'when user_name is provided' do
        subject(:get_details_for_user) { jpost authn_user_details_path, { user_name: user_2.name } }

        include_examples 'returns with user details'
      end

      context 'when email_address is provided' do
        subject(:get_details_for_user) { jpost authn_user_details_path, { email_address: user_2.email } }

        include_examples 'returns with user details'
      end

      context 'when matching user is not found' do
        subject(:get_details_for_user) { jpost authn_user_details_path, { email_address: 'non-existant user' } }

        it 'returns with an empty hash' do
          get_details_for_user

          expect(response).to have_http_status(:ok)
          expect(parsed_body).to eq({})
        end
      end
    end

    context 'with invalid token' do
      subject(:get_details_for_user) { jpost authn_user_details_path, { user_name: user_2.name } }

      it 'fails to access user details' do
        get_details_for_user

        expect(response).to have_http_status(:unauthorized)
        expect(parsed_body).to include("errors"=>["You need to sign in or sign up before continuing."])
      end
    end
  end
end
