require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'group assigned users' do
    let(:group) { FactoryGirl.create(:group) }
    let(:user_1) { FactoryGirl.create(:user, groups: [group]) }
    let(:user_2) { FactoryGirl.create(:user, groups: [group]) }

    it "has users" do
      expect(group.users).to include(user_1, user_2)
    end
  end

  context 'group with name' do
    let!(:group_1) { FactoryGirl.create(:group, name: 'testgroup') }

    it 'is unique' do
      expect{ FactoryGirl.create(:group, name: group_1.name) }.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end
end
