require 'rails_helper'

RSpec.describe User, type: :model do
  include_context 'db_cleanup_each', :transaction

  shared_context 'user assigned to admin group' do
    let(:admin_group) { FactoryGirl.create(:group, name: Group::ADMIN) }
    let!(:user) { FactoryGirl.create(:user, groups: [admin_group]) }
  end

  shared_context 'user assigned to non-admin group' do
    let(:non_admin_group) { FactoryGirl.create(:group, name: 'member') }
    let!(:user) { FactoryGirl.create(:user, groups: [non_admin_group]) }
  end

  context 'user assigned groups' do
    let(:user) { FactoryGirl.create(:user) }

    it "has groups" do
      user.groups.create(:name=>Group::ADMIN)
      user.groups.create(:name=>'member')

      db_user = User.find(user.id)

      expect(db_user.has_groups([Group::ADMIN, 'member'])).to be true
    end
  end

  describe '#is_admin?' do
    context 'user assigned to admin group' do
      include_context 'user assigned to admin group'

      it 'returns true' do
        expect(user.is_admin?).to be true
      end
    end

    context 'user assigned to non-admin group' do
      include_context 'user assigned to non-admin group'

      it 'returns false' do
        expect(user.is_admin?).to be false
      end
    end
  end

  describe '#create_group' do
    context 'user assigned to admin group' do
      include_context 'user assigned to admin group'

      it 'can create new groups' do
        expect { user.create_group('new_group') }.to change { Group.count }.by(1)
      end
    end

    context 'user assigned to non-admin group' do
      include_context 'user assigned to non-admin group'

      it 'cannot create new groups' do
        expect { user.create_group('new_group') }.not_to change { Group.count }
      end
    end
  end

  describe '#assign_group' do
    context 'user assigned to admin group' do
      include_context 'user assigned to admin group'
      let(:user_2) { FactoryGirl.create(:user) }

      it 'can assign other users to groups' do
        Group.create(name: 'new_group')
        expect { user.assign_group('new_group', user_2.name) }.to change { user_2.groups.count }.by(1)
      end
    end

    context 'user assigned to non-admin group' do
      include_context 'user assigned to non-admin group'
      let(:user_2) { FactoryGirl.create(:user) }

      it 'cannot assign other users to groups' do
        Group.create!(name: 'new_group')
        expect { user.assign_group('new_group', user_2.name) }.not_to change { user_2.groups.count }
      end
    end
  end

  describe '#details' do
    let(:group_1) { FactoryGirl.create(:group, name: 'member') }
    let(:group_2) { FactoryGirl.create(:group, name: 'general_user') }
    let!(:user) { FactoryGirl.create(:user, groups: [group_1, group_2]) }

    it 'returns with username, email address and group memberships' do
      expect(user.details.keys).to eq([:user_name, :email_address, :group_memberships])
      expect(user.details.values).to eq([user.name, user.email, user.groups.map(&:name)])
    end
  end
end
